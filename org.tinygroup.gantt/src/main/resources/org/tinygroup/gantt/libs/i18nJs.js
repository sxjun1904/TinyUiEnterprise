


  function dateToRelative(localTime){
    var diff=new Date().getTime()-localTime;
    var ret="";

    var min=60000;
    var hour=3600000;
    var day=86400000;
    var wee=604800000;
    var mon=2629800000;
    var yea=31557600000;

    if (diff<-yea*2)
      ret ="##年内".replace("##",(-diff/yea).toFixed(0));

    else if (diff<-mon*9)
      ret ="##月内".replace("##",(-diff/mon).toFixed(0));

    else if (diff<-wee*5)
      ret ="##周内".replace("##",(-diff/wee).toFixed(0));

    else if (diff<-day*2)
      ret ="##天内".replace("##",(-diff/day).toFixed(0));

    else if (diff<-hour)
      ret ="##小时内".replace("##",(-diff/hour).toFixed(0));

    else if (diff<-min*35)
      ret ="1小时内";

    else if (diff<-min*25)
      ret ="半小时内";

    else if (diff<-min*10)
      ret ="几分钟内";

    else if (diff<-min*2)
      ret ="马上";

    else if (diff<=min)
      ret ="刚刚";

    else if (diff<=min*5)
      ret ="5分钟前";

    else if (diff<=min*15)
      ret ="几分钟前";

    else if (diff<=min*35)
      ret ="半小时前";

    else if (diff<=min*75)
      ret ="1小时前";

    else if (diff<=hour*5)
      ret ="5小时前";

    else if (diff<=hour*24)
      ret ="##小时前".replace("##",(diff/hour).toFixed(0));

    else if (diff<=day*7)
      ret ="##天前".replace("##",(diff/day).toFixed(0));

    else if (diff<=wee*5)
      ret ="##周前".replace("##",(diff/wee).toFixed(0));

    else if (diff<=mon*12)
      ret ="##月前".replace("##",(diff/mon).toFixed(0));

    else
      ret ="##年前".replace("##",(diff/yea).toFixed(0));

    return ret;
  }

  //override date format i18n

  Date.monthNames = ["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"];
  // Month abbreviations. Change this for local month names
  Date.monthAbbreviations = ["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"];
  // Full day names. Change this for local month names
  Date.dayNames =["星期日","星期一","星期二","星期三","星期四","星期五","星期六"];
  // Day abbreviations. Change this for local month names
  Date.dayAbbreviations = ["日","一","二","三","四","五","六"]
  // Used for parsing ambiguous dates like 1/2/2000 - default to preferring 'American' format meaning Jan 2.
  // Set to false to prefer 'European' format meaning Feb 1
  Date.preferAmericanFormat = false;

  Date.firstDayOfWeek =0;
  Date.defaultFormat = "yyyy-MM-dd";//dd/MM/yyyy";


  Number.decimalSeparator = ".";
  Number.groupingSeparator = ",";
  Number.minusSign = "-";
  Number.currencyFormat = "##0.00";



  var millisInWorkingDay =36000000;
  var workingDaysPerWeek =5;

  function isHoliday(date) {
    var friIsHoly =false;
    var satIsHoly =true;
    var sunIsHoly =true;

    pad = function (val) {
      val = "0" + val;
      return val.substr(val.length - 2);
    };

    var holidays = "#01_01#04_25#08_15#11_01#12_25#12_26#06_02#12_08#05_01#2010_04_05#2010_10_19#2010_05_15#2011_04_04#";

    var ymd = "#" + date.getFullYear() + "_" + pad(date.getMonth() + 1) + "_" + pad(date.getDate()) + "#";
    var md = "#" + pad(date.getMonth() + 1) + "_" + pad(date.getDate()) + "#";
    var day = date.getDay();

    return  (day == 5 && friIsHoly) || (day == 6 && satIsHoly) || (day == 0 && sunIsHoly) || holidays.indexOf(ymd) > -1 || holidays.indexOf(md) > -1;
  }



  var i18n = {
    FORM_IS_CHANGED:"页面还没保存!",
    YES:"是",
    NO:"否",
    FLD_CONFIRM_DELETE:"确定要删除吗?",
    INVALID_DATA:"格式不正确",//The data inserted are invalid for the field format.",
    ERROR_ON_FIELD:"输入出错",
    CLOSE_ALL_CONTAINERS:"关闭吗?",



    DO_YOU_CONFIRM:"请确认?"
  };

