##Percentage:百分比 比如25%
#macro simpleProgressBar(simpleProgressBarPercentage)
	<div class="progress-unique">
        <div class="progress-bars"><span class="sr-modern" style="#if(simpleProgressBarPercentage)width:$!{simpleProgressBarPercentage}#end"></span></div>
    </div>
#end

#macro progressAria(progressAriaPoint progressAriaClass)
	<div#if(progressAriaPoint) aria-valuetransitiongoal="$!{progressAriaPoint}" aria-valuenow="$!{progressAriaPoint}"  style="width: $!{progressAriaPoint}%;"#{end}#if(progressAriaClass) class="$!{progressAriaClass}"#end> 
    	#bodyContent
    </div>
#end

#macro progressData(proDataPlacement dataOriginalTitle progressRef progressClass)
	<div#if(proDataPlacement) data-placement="$!{proDataPlacement}"#{end}#if(dataOriginalTitle) data-original-title="$!{dataOriginalTitle}"#{end}#if(progressRef) rel="$!{progressRef}"#{end} class="progress right $!{progressClass}">
		#bodyContent
	</div>
#end

#macro barHolderMenu()
	<div class="well no-padding">
		#bodyContent
	</div>
#end

#*
	barHolderPercentage:百分率。比如25，表示25%
	barHolderColorCls:颜色对应class
*#
#macro barHolder(barHolderPercentage barHolderColorCls)
	<div class="bar-holder">
		<div class="progress right">
			<div aria-valuetransitiongoal="$!{barHolderPercentage}" class="progress-bar $!{barHolderColorCls}" style="width: $!{barHolderPercentage}%;" aria-valuenow="$!{barHolderPercentage}">${barHolderPercentage}%</div>
		</div>
	</div>
#end

#macro progressFrame(progressFrameCls)
	<div class="progress $!{progressFrameCls}">
		#bodyContent
	</div>
#end
#*
	progressBarPercentage:百分率 
	progressBarSizeCls:大小class
	progressBarColorCls:颜色class
*#
#macro progressBar(progressBarPercentage progressBarSizeCls progressBarColorCls)
	#@progressFrame(progressBarSizeCls)
		<div style="width: $!{progressBarPercentage}%;" role="progressbar" class="progress-bar $!{progressBarColorCls}"></div>
	#end
#end


#*miniProgressBarPercentage:百分率*#
#macro miniProgressBar(miniProgressBarPercentage miniProgressBarColorCls)
	#progressBar(miniProgressBarPercentage "progress-micro" miniProgressBarColorCls)
#end

#*超小进度条*#
#macro xsProgressBar(xsProgressBarPercentage xsProgressBarColorCls)
	#progressBar(xsProgressBarPercentage "progress-xs" "bg-color-blue")
#end

#*小进度条*#
#macro smProgressBar(smProgressBarPercentage smProgressBarColorCls)
	progressBar(smProgressBarPercentage "progress-sm" smProgressBarColorCls)
#end

#*默认进度条*#
#macro defaultProgressBar(defaultProgressBarPercentage defaultProgressBarColorCls)
	#progressBar(defaultProgressBarPercentage "" defaultProgressBarColorCls)
#end

#*大进度条*#
#macro lgProgressBar(lgProgressBarPercentage lgProgressBarColorCls)
	#progressBar(lgProgressBarPercentage "progress-lg" lgProgressBarColorCls)
#end

##斜纹进度条
#macro stripedProgress(stripedProgressPercentage stripedProgressColorCls)
		#progressBar(stripedProgressPercentage "progress-striped" stripedProgressColorCls)
#end

#macro stripedActiveProgress(stripedActiveProgressPercentage stripedActiveProgressColorCls)
	#progressBar(stripedActiveProgressPercentage "progress-sm progress-striped active" stripedActiveProgressColorCls)
#end

##分段
#macro sectionProgress(sectionProgressPercentage sectionProgressColorCls)
	<div style="width: $!{sectionProgressPercentage}%" class="progress-bar $!{sectionProgressColorCls}"></div>
#end

#macro processTip(processTipLeftTxt processTipRightTxt)
	<p>
		<span class="label label-info">${processTipLeftTxt}</span> #bodyContent<span class="txt-color-purple pull-right"><i class="fa fa-warning"></i> ${processTipRightTxt}</span>
	</p>
#end

#macro processRightInfo(processRightInfoCls processRightInfoIconCls)
	<span class="$!{processRightInfoCls} pull-right"><i class="fa $!{processRightInfoIconCls}"></i></span>
#end